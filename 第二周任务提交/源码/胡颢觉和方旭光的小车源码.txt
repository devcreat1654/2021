int flag = 1;
int REDA = 3;
int LEDA = 9;
int L1 = 2;
int L2 = 4;
int R1 = 6;
int R2 = 5;
const int XJA1 = A0;
const int XJA2 = A1;
const int XJA3 = A2;
const int XJA4 = A3;
const int XJA5 = A4;
const int XJB1 = A5;
const int XJB2 = A6;
int st1;
int st2;
int st3;
int st4;
int st5;
int st6;
int st7;

void setup()
{
  // put your setup code here, to run once:
  pinMode(1, INPUT);
  pinMode(0, INPUT);
  pinMode(XJA1, INPUT);
  pinMode(XJA2, INPUT);
  pinMode(XJA3, INPUT);
  pinMode(XJA4, INPUT);
  pinMode(XJA5, INPUT);
  pinMode(XJB1, INPUT);
  pinMode(L1, OUTPUT);
  pinMode(L2, OUTPUT);
  pinMode(R1, OUTPUT);
  pinMode(R2, OUTPUT);
  pinMode(REDA, OUTPUT);
  pinMode(LEDA, OUTPUT);
  digitalWrite(XJA1, HIGH);
  digitalWrite(XJA2, HIGH);
  digitalWrite(XJA3, HIGH);
  digitalWrite(XJA4, HIGH);
  digitalWrite(XJA5, HIGH);
  digitalWrite(XJB1, HIGH);
  Serial.begin(9600);
}

void forward()
{
  analogWrite(REDA, 80);
  analogWrite(LEDA, 80);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void back()
{
  analogWrite(REDA, 80);
  analogWrite(LEDA, 80);
  digitalWrite(L1, LOW);
  digitalWrite(L2, HIGH);
  digitalWrite(R1, LOW);
  digitalWrite(R2, HIGH);
}

void left_1()
{
  analogWrite(REDA, 80);
  analogWrite(LEDA, 10);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}

void left_2()
{
  analogWrite(REDA, 100);
  analogWrite(LEDA, 10);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void left_3()
{
  analogWrite(REDA, 150);
  analogWrite(LEDA, 10);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void left_4()
{
  analogWrite(REDA, 60);
  analogWrite(LEDA, 100);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}

void right_1()
{
  analogWrite(REDA, 10);
  analogWrite(LEDA, 70);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void right_2()
{
  analogWrite(REDA, 10);
  analogWrite(LEDA, 100);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void right_3()
{
  analogWrite(REDA, 10);
  analogWrite(LEDA, 150);
  digitalWrite(L1, HIGH);
  digitalWrite(L2, LOW);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
}
void right_4()
{
  analogWrite(REDA, 60);
  analogWrite(LEDA, 100);
  digitalWrite(R1, HIGH);
  digitalWrite(R2, LOW);
  digitalWrite(L1, LOW);
  digitalWrite(L2, HIGH);
}

void STOP()
{
  digitalWrite(L1, LOW);
  digitalWrite(L2, LOW);
  digitalWrite(R1, LOW);
  digitalWrite(R2, LOW);
}

void xunji()
{
  st1 = digitalRead(XJA1);
  st2 = digitalRead(XJA2);
  st3 = digitalRead(XJA3);
  st4 = digitalRead(XJA4);
  st5 = digitalRead(XJA5);
  st6 = digitalRead(XJB1);
}

void train()//输入T进入的循迹模式
{
  while (Serial.available() == 0)
  {
    xunji();//探测循迹的输入
    if (st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == HIGH)
    {
      Serial.println("RLST");
      delay(200);
      RLST();//衔接的备用循迹模式
    }
    else//正常的循迹模式
    {
      xunji();//探测循迹的输入
      if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == LOW && st5 == LOW )//1,2,3,4,5亮
      {
        back();
        Serial.println("back");
      }


      else if (st1 == LOW && st2 == HIGH && st3 == LOW && st4 == LOW && st5 == LOW || st1 == LOW && st2 == HIGH && st3 == HIGH && st4 == LOW && st5 == LOW )
      {
        left_1();
        Serial.println("left1");

      }
      else if (st1 == HIGH && st2 == HIGH && st3 == LOW && st4 == LOW && st5 == LOW || st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == LOW)
      {
        left_2();
        Serial.println("left2");
      }
      else if (st1 == HIGH && st2 == LOW && st3 == LOW && st4 == LOW && st5 == LOW || st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == LOW && st5 == LOW)
      {
        left_3();
        Serial.println("left3");

      }



      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == HIGH && st5 == LOW || st1 == LOW && st2 == LOW && st3 == HIGH && st4 == HIGH && st5 == LOW  )
      {
        right_1();
        Serial.println("right1");
      }
      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == HIGH && st5 == HIGH || st1 == LOW && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == HIGH)
      {
        right_2();
        Serial.println("right_2");
      }
      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == LOW && st5 == HIGH || st1 == LOW && st2 == LOW && st3 == HIGH && st4 == HIGH && st5 == HIGH )
      {

        right_3();
        Serial.println("right_3");

      }
      else
      {
        forward();
        Serial.println("run");
      }
    }
  }
}
void XJB()
{
  xunji();
  right_4();
  Serial.println("right_4");
  if (st1 == HIGH || st2 == HIGH || st3 == HIGH || st4 == HIGH || st5 == HIGH)
  {
    RLST();
  }
  else
  {
    XJB();
  }
}
void RLST()//衔接的循迹函数
{
  while (Serial.available() == 0)//一般情况函数离不开循环，只能借助break函数
  {
    xunji();//探测循迹的输入
    if (st6 == LOW)
    {
      Serial.println("XJB");
      XJB();
    }


    else//一路循迹无检测到黑线，延续现有的循迹模式
    {
      xunji();//探测循迹的输入
      if (st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == HIGH )
      {
        STOP();
        Serial.println("stop");
        break; vgg  gggg g
      }

      else if (st1 == LOW && st2 == HIGH && st3 == LOW && st4 == LOW && st5 == LOW || st1 == LOW && st2 == HIGH && st3 == HIGH && st4 == LOW && st5 == LOW )
      {
        left_1();
        Serial.println("left1");

      }
      else if (st1 == HIGH && st2 == HIGH && st3 == LOW && st4 == LOW && st5 == LOW || st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == LOW)
      {
        left_2();
        Serial.println("left2");
      }
      else if (st1 == HIGH && st2 == LOW && st3 == LOW && st4 == LOW && st5 == LOW || st1 == HIGH && st2 == HIGH && st3 == HIGH && st4 == LOW && st5 == LOW)
      {
        left_3();
        Serial.println("left3");

      }



      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == HIGH && st5 == LOW || st1 == LOW && st2 == LOW && st3 == HIGH && st4 == HIGH && st5 == LOW  )
      {
        right_1();
        Serial.println("right1");
      }
      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == HIGH && st5 == HIGH || st1 == LOW && st2 == HIGH && st3 == HIGH && st4 == HIGH && st5 == HIGH)
      {
        right_2();
        Serial.println("right_2");
      }
      else if (st1 == LOW && st2 == LOW && st3 == LOW && st4 == LOW && st5 == HIGH || st1 == LOW && st2 == LOW && st3 == HIGH && st4 == HIGH && st5 == HIGH )
      {

        right_3();
        Serial.println("right_3");

      }

      else
      {
        forward();
        Serial.println("run");
      }
    }
  }
}

void wireless()
{
  while (Serial.available() == 0);
  int Byte = Serial.read();
  if (Byte == 'G')
  {
    forward();
  }
  else if (Byte == 'H')
  {
    left_2();
  }
  else if (Byte == 'J')
  {
    right_2();
  }
  else if (Byte == 'K')
  {
    back();
  }
  else
    STOP();

  Serial.flush();
}

void loop()
{
  while (Serial.available() == 0);
  int Byte = Serial.read();
  if (Byte == '1')
  {
    wireless();
  }
  else
  {
    train();
  }
  Serial.flush();
}