蓝牙遥控部分


#define IN1 5 // 右轮
#define IN2 6 
#define IN3 9 // 左轮
#define IN4 10 

#define LEFT '3' //左转编码
#define RIGHT '4'//右转编码
#define GO '1'//前进编码
#define BACK '2'//后退编码
#define STOP '0'//停止编码

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(IN1,OUTPUT);
  pinMode(IN2,OUTPUT);
  pinMode(IN3,OUTPUT);
  pinMode(IN4,OUTPUT);
  initCar();
}

void loop() {
  // put your main code here, to run repeatedly:
  if(Serial.available()>0){
      char ch = Serial.read();
      if(ch == GO){
         //前进
         go();
      }else if(ch == BACK){
         //后退
         back();
      }else if(ch == LEFT){
         //左转
         turnLeft();
      }else if(ch == RIGHT){
        //右转
        turnRight();
      }else if(ch=='0'){
        //停车
        stopCar();
      }
    }
}

void initCar(){
  //默认全是低电平 停止状态
  digitalWrite(IN1,LOW);      
  digitalWrite(IN2,LOW);
  digitalWrite(IN3,LOW);   
  digitalWrite(IN4,LOW);
}

/**
* 左转
*/
void turnLeft(){
  digitalWrite(IN1,HIGH);      
  digitalWrite(IN2,LOW);         //右轮前进
  digitalWrite(IN3,LOW);      
  digitalWrite(IN4,LOW);         //左轮不动
}

/**
* 右转
*/
void turnRight(){
  digitalWrite(IN1,LOW);      
  digitalWrite(IN2,LOW);         //右轮不动
  digitalWrite(IN3,HIGH);      
  digitalWrite(IN4,LOW);         //左轮前进
}

/**
* 前进
*/
void go(){
  digitalWrite(IN1,HIGH);      
  digitalWrite(IN2,LOW);         //右轮前进
  digitalWrite(IN3,HIGH);      
  digitalWrite(IN4,LOW);         //左轮前进
}

/**
* 倒车
*/
void back(){
  digitalWrite(IN1,LOW);      
  digitalWrite(IN2,HIGH);        //右轮后退
  digitalWrite(IN3,LOW);      
  digitalWrite(IN4,HIGH);        //左轮后退
}

/**
* 停车
*/
void stopCar(){
  initCar();
}






循迹部分




#define STOP 0
#define FORWARD 1
#define BACKWARD 2
#define TURNLEFT 3
#define TURNRIGHT 4
#define TURNLEFT2 5
int S=0;
const int L298pin1 = 5; // 控制左右两个电机
const int L298pin2 = 6;
const int L298pin3 = 9;
const int L298pin4 = 10;
const int ENA = 3;//控制PWM
const int ENB = 11;
const int sensor1 = 2;//五路传感器
const int sensor2 = 7;
const int sensor3 = 4;
const int sensor4 = 12;
const int sensor5 = 13;


void setup() 
{ 
    Serial.begin(9600); 
    pinMode(L298pin1,OUTPUT);
    pinMode(L298pin2,OUTPUT);
    pinMode(L298pin3,OUTPUT);
    pinMode(L298pin4,OUTPUT);
    
    pinMode(ENA,OUTPUT);
    pinMode(ENB,OUTPUT);
    
    pinMode(sensor1,INPUT);
    pinMode(sensor2,INPUT);
    pinMode(sensor3,INPUT);
    pinMode(sensor4,INPUT);
    pinMode(sensor5,INPUT);
} 

void loop()
{
  tracing();
}
void motorRun(int cmd,int value)
{
analogWrite(ENA, value); 
analogWrite(ENB, value);
switch(cmd){
case FORWARD:
  digitalWrite(L298pin1,HIGH);
  digitalWrite(L298pin2,LOW);
  digitalWrite(L298pin3,LOW);
  digitalWrite(L298pin4,HIGH);
break;
case BACKWARD:
  digitalWrite(L298pin1,LOW);
  digitalWrite(L298pin2,HIGH);
  digitalWrite(L298pin3,HIGH);
  digitalWrite(L298pin4,LOW);
break;
case TURNLEFT:
  digitalWrite(L298pin1,LOW);
  digitalWrite(L298pin2,HIGH);
  digitalWrite(L298pin3,LOW);
  digitalWrite(L298pin4,HIGH);
break;
case TURNRIGHT:
  digitalWrite(L298pin1,HIGH);
  digitalWrite(L298pin2,LOW);
  digitalWrite(L298pin3,HIGH);
  digitalWrite(L298pin4,LOW);
break;
case TURNLEFT2:
  digitalWrite(L298pin1,LOW);
  digitalWrite(L298pin2,LOW);
  digitalWrite(L298pin3,LOW);
  digitalWrite(L298pin4,HIGH);
break;
default:
  digitalWrite(L298pin1,LOW);
  digitalWrite(L298pin2,LOW);
  digitalWrite(L298pin3,LOW);
  digitalWrite(L298pin4,LOW);
}
}

void tracing()
{
int data[5];
data[0] = digitalRead(sensor1);
data[1] = digitalRead(sensor2);
data[4] = digitalRead(sensor3);
data[2] = digitalRead(sensor4);
data[3] = digitalRead(sensor5);

if(!data[0] && !data[1] && !data[2] && !data[3]&&data[4]) //左右都没有检测到
{
  motorRun(FORWARD,65);
 delay(100);
}
if(data[2] && !data[3]&&!data[0]&&!data[1]) //右边检测到黑线小修正
{
  motorRun(TURNRIGHT,60);
}
if(data[2] && data[3]&&!data[0]&&!data[1]) //右边检测到黑线大转弯
{ 
  motorRun(FORWARD,45);
  delay(100);
  motorRun(TURNRIGHT,70);
     delay(250);
}
if(!data[0] && data[1]&&!data[2]&&!data[3]) //左边检测到黑线小修正
{
  motorRun(TURNLEFT,45);
}
if(!data[0] && data[1]&&!data[2]&&!data[3]) //左边检测到黑线大转弯
{
   motorRun(FORWARD,55);
   delay(60);
   motorRun(TURNLEFT,60);
   delay(250);
}

 if(data[0] && data[1] && data[2] && data[3]&&data[4]) //左右都检测到黑线是停止
{
  switch(S){
    case 0: motorRun(FORWARD,60);
            delay(800);
            S=1;
            break;
    case 1:motorRun(FORWARD,50);
            delay(500);
            motorRun(TURNRIGHT,60);
            delay(900);
            S=2;
            break;
  }
  motorRun(STOP, 0);

}
if(!data[0] && !data[1] && !data[2] && !data[3]&&!data[4]){
  motorRun(BACKWARD,40);
delay(70);
}
}